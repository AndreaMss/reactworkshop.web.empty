import React, { Component } from 'react';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="content">
        Welcome to our React Workshop!
      </div>
    );
  }
}

export default App;
